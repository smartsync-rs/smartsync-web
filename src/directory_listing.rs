use std::{error::Error, fs, path::Path};

use serde::Serialize;

#[derive(Serialize)]
pub struct DirectoryListing {
    pub parent: Option<String>,
    pub directory: String,
    pub files: Vec<DirectoryItem>,
}

#[derive(Serialize)]
pub struct DirectoryItem {
    pub path: String,
    pub name: String,
    pub is_dir: bool,
}

impl DirectoryListing {
    pub fn new(directory: &str) -> Result<DirectoryListing, Box<dyn Error>> {
        let path = Path::new(directory);
        let parent = path
            .parent()
            .map(|parent| parent.to_str().unwrap().to_string());
        let mut files = Vec::new();
        if !path.is_dir() {
            return Err(format!("{} is not a directory", directory).into());
        }
        for entry in fs::read_dir(path)? {
            let entry_path = entry?.path();
            if let Some(name) = entry_path.file_name() {
                if let Some(name) = name.to_str() {
                    files.push(DirectoryItem {
                        path: entry_path.to_str().unwrap().to_string(),
                        name: name.to_string(),
                        is_dir: entry_path.is_dir(),
                    });
                }
            }
        }

        Ok(DirectoryListing {
            parent,
            directory: directory.to_string(),
            files,
        })
    }
}
