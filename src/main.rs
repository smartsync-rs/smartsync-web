use rocket::{http::Status, response::status};
use rocket_dyn_templates::Template;

mod directory_listing;
mod summary;

#[rocket::get("/")]
fn index() -> Result<Template, status::Custom<String>> {
    match summary::Summary::new() {
        Ok(context) => Ok(Template::render("index", context)),
        Err(error) => Err(status::Custom(
            Status::InternalServerError,
            error.to_string(),
        )),
    }
}

#[rocket::get("/file-browser/<path>")]
fn file_browser(path: String) -> Result<Template, status::Custom<String>> {
    match directory_listing::DirectoryListing::new(&path) {
        Ok(context) => Ok(Template::render("file-browser", context)),
        Err(error) => Err(status::Custom(
            Status::InternalServerError,
            error.to_string(),
        )),
    }
}

#[rocket::launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", rocket::routes![index, file_browser])
        .attach(Template::fairing())
}
