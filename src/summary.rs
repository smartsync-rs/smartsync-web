use std::error::Error;

use serde::Serialize;
use smartsync_core::{config, registry};

#[derive(Serialize)]
pub struct Summary {
    backups: Vec<BackupSummary>,
}

#[derive(Serialize)]
struct BackupSummary {
    name: String,
    path: String,
    devices: Vec<DeviceSummary>,
}

#[derive(Serialize)]
struct DeviceSummary {
    name: String,
    last_backup: String,
    files: Vec<FileSyncSummary>,
}

#[derive(Serialize)]
struct FileSyncSummary {
    name: String,
    sources: Vec<String>,
    dest: String,
}

impl Summary {
    pub fn new() -> Result<Summary, Box<dyn Error>> {
        let mut backups = Vec::new();
        let reg = registry::load_registry()?;
        for backup in reg.backups {
            let cfg = config::load_config(&backup.path)?;
            let mut devices = Vec::new();
            for device_cfg in cfg.devices {
                let last_backup = if let Some(timestamp) = device_cfg.last_backup {
                    format!("{}", timestamp)
                } else {
                    "N/A".to_string()
                };
                let mut files = Vec::new();
                for fsync in device_cfg.files {
                    files.push(FileSyncSummary {
                        name: fsync.name.clone(),
                        sources: fsync.sources_str(),
                        dest: fsync.dest_str(),
                    });
                }
                devices.push(DeviceSummary {
                    name: device_cfg.name.clone(),
                    last_backup,
                    files,
                });
            }
            backups.push(BackupSummary {
                name: backup.name.clone(),
                path: backup.path_str(),
                devices,
            });
        }
        Ok(Summary { backups })
    }
}
